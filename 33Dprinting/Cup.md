# 3D Printing for A Cup
## Step 1 设置3D打印所需要的文件和参数

![](https://gitlab.com/jennyius/ius/uploads/e7abba35ce24a3c8c76e629fe6dd39fa/20200818163039.png)

设置杯壁厚度

![](https://gitlab.com/jennyius/ius/uploads/e5b2f5e600a1fef23f2bea8752a172a9/20200818163118.png)

设置填充密度参数

![](https://gitlab.com/jennyius/ius/uploads/d743cea63763c328f6de9b292702fcb1/20200818163225.png)

设置支撑参数

![](https://gitlab.com/jennyius/ius/uploads/d71c4bc02f8e637e615b0d86ca7923e4/20200818163307.png)

## Step 2 设置3D打印机和设备

准备打印板，贴上一层磨砂纸增加摩擦避免打印过程中移位

![](https://gitlab.com/jennyius/ius/uploads/87541d469a1d2226987fab8a0d49fd56/20200818163539.png)

第一次打印因为打印板面太光滑，导致打印移位，失败

![](https://gitlab.com/jennyius/ius/uploads/1c1999188e27e806a7a0a673c1df201b/%E9%80%8F%E6%98%8E.jpeg)

做好打印机参数设定

![](https://gitlab.com/jennyius/ius/uploads/c1312bfdffe3b27df51b9e74c88d53bb/20200818165001.png)

[Setting video 1](https://www.youtube.com/watch?v=ljCm2cSy9ms)
[Setting video 2](https://www.youtube.com/watch?v=8exExDxCfdE)

观察打印是否准确进行

![](https://gitlab.com/jennyius/ius/uploads/b759dfccd0f27ea4a9259ccbcad9c927/20200818165110.png)
[3D printing](https://www.youtube.com/watch?v=8--DMjS2_Y8)

打印结束

![](https://gitlab.com/jennyius/ius/uploads/7627cc314283a85d3d6ee55c1c22e932/20200818165718.png)

## Step 3 打印后处理

去掉多余的支撑

![](https://gitlab.com/jennyius/ius/uploads/e19aefd2b76578fabd5e6cb5570276b5/20200820140555.png)
[Related video](https://www.youtube.com/watch?v=8B-xUfc0UPU)

由于杯身有拔模斜度，导致杯盖和杯身尺寸不契合，后期手动调整尺寸

![](https://gitlab.com/jennyius/ius/uploads/1fc328be66a39d1bbb079c6c02c920b2/20200818165545.png)
![](https://gitlab.com/jennyius/ius/uploads/bef8c3eb227ce0ac2c4337e6cdfd88c3/20200818165607.png)

## 最后成品

![](https://gitlab.com/jennyius/ius/uploads/e11e71f5e262ab9b02af92b08ca42f7e/20200818165647.png)
![](https://gitlab.com/jennyius/ius/uploads/b89ce67ad60684f0a2a92f399452f144/20200818165806.png)

## 文档存储
https://drive.google.com/drive/folders/13JrjvTj6ZpqTnCxa42t5OUg4NEqsbNBz

[jenny](https://www.linkedin.com/feed/)